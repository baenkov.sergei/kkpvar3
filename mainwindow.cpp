#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions.h"
#include "error.h"

/**
*\file
 *\brief
 *\author Sergei Baenkov
 *\version 1.0
*/

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setError(QString err)
{
    Error* window = new Error();
    window->changeText(err);
    window->exec();
}

void MainWindow::on_amountButton_clicked()
{
    QString str = ui->amountInput->toPlainText();
    QString output;
    if (amountOfCharacter(str) == -1)
    {
        setError("Строка пуста");
        return;
    }
    else if (amountOfCharacter(str) == 0)
    {
        setError("Нет киррилических символов");
        return;
    }
    else
    {
        output = "Общее кол-во букв:" + QString::number(amountOfCharacter(str));
    }
    output = output + "\nКол-во гласных:" + QString::number(amountOfVowels(str));
    output = output + "  Кол-во согласных:" + QString::number(amountOfConsonants(str));
    ui->amountOutput->setText(output);
}

void MainWindow::on_palindromeButton_clicked()
{
    QString str = ui->palindromeInput->toPlainText();
    int result = isPalindrome(str);
    switch(result)
    {
        case -2:
        {
            setError("Строка пуста");
            return;
        }
        case -1:
        {
            setError("\nДопустимы буквы киррилического\nалфавита символы:'.''!''?'',''-',\n а также пробел");
            return;
        }
        case 0:
        {
            ui->palindromeOutput->setText("Данное предложение не является палиндромом");
            return;
        }
        case 1:
        {
            ui->palindromeOutput->setText("Данное предложение является палиндромом");
            return;
        }
    }
}

void MainWindow::on_encryptButton_clicked()
{
    QString str = ui->encryptInput->toPlainText();
    QString numberStr = ui->encruptInputLine->text();
    bool ok = false;
    int number = numberStr.toInt(&ok);
    int result = encryption(str,number);
    if (!ok)
    {
        setError("Некорректные символы\n в строке числа U");
        return;
    }
    switch (result)
    {
        case 0:
        {
            setError("Исходная строка пуста!");
            return;
        }
        case -2:
        {
            setError("Чисто U должно быть\n больше или равно нуля!");
            return;
        }
        case -1:
        {
            setError("Исходная строка\n содержит недопустимые символы!");
            return;
        }
        case 1:
        {
            ui->encryptOutput->setText(str);
            return;
        }
    }
}

void MainWindow::on_roundingButton_clicked()
{
    QString str = ui->roundingInput->text();
    QString numberStr = ui->roundingInputLine->text();
    bool ok = false;
    int number = numberStr.toInt(&ok,10);
    QString output;
    if (!ok)
    {
        setError("Некорректные символы в\nстроке числа знаков после запятой!");
        return;
    }
    int result = shorteringNumber(str,number);
    switch (result)
    {
        case 0:
        {
            setError("Исходная строка пуста!");
            return;
        }
        case -2:
        {
            setError("Некорректное число знаков после запятой!");
            return;
        }
        case -1:
        {
            setError("В строке недопустимые символы");
            return;
        }
        case 1:
        {
            ui->roundingOutput->setText("");
            ui->roundingOutput->setText(str);
            return;
        }
        case -3:
        {
            setError("+ или - стоят не на первом месте");
            return;
        }
        case -4:
        {
            setError("В строке больше одного + или -");
            return;
        }
        case -5:
        {
            setError("В строке больше одной\n или ни одной точки");
            return;
        }
        case -6:
        {
            setError("В исходной строке слишком много символов");
            return;
        }
    }
}
