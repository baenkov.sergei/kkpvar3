/********************************************************************************
** Form generated from reading UI file 'error.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ERROR_H
#define UI_ERROR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Error
{
public:
    QLabel *label;
    QLabel *errorStr;
    QPushButton *errorButton;

    void setupUi(QDialog *Error)
    {
        if (Error->objectName().isEmpty())
            Error->setObjectName(QString::fromUtf8("Error"));
        Error->resize(400, 300);
        label = new QLabel(Error);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(160, 60, 111, 31));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        errorStr = new QLabel(Error);
        errorStr->setObjectName(QString::fromUtf8("errorStr"));
        errorStr->setGeometry(QRect(20, 90, 361, 111));
        errorStr->setFont(font);
        errorStr->setAlignment(Qt::AlignCenter);
        errorButton = new QPushButton(Error);
        errorButton->setObjectName(QString::fromUtf8("errorButton"));
        errorButton->setGeometry(QRect(110, 220, 171, 51));
        errorButton->setFont(font);

        retranslateUi(Error);

        QMetaObject::connectSlotsByName(Error);
    } // setupUi

    void retranslateUi(QDialog *Error)
    {
        Error->setWindowTitle(QApplication::translate("Error", "Dialog", nullptr));
        label->setText(QApplication::translate("Error", "\320\236\321\210\320\270\320\261\320\272\320\260!", nullptr));
        errorStr->setText(QApplication::translate("Error", "\320\236\321\210\320\270\320\261\320\272\320\260!", nullptr));
        errorButton->setText(QApplication::translate("Error", "\320\227\320\260\320\272\321\200\321\213\321\202\321\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Error: public Ui_Error {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ERROR_H
