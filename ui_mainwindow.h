/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *amount;
    QTextEdit *amountInput;
    QPushButton *amountButton;
    QLabel *label;
    QLabel *amountOutput;
    QLabel *label_6;
    QWidget *palindrome;
    QPushButton *palindromeButton;
    QLabel *label_2;
    QLabel *palindromeOutput;
    QTextEdit *palindromeInput;
    QLabel *label_7;
    QWidget *encrypt;
    QPushButton *encryptButton;
    QTextEdit *encryptInput;
    QLabel *label_3;
    QLabel *label_4;
    QTextBrowser *encryptOutput;
    QLineEdit *encruptInputLine;
    QLabel *label_5;
    QWidget *rounding;
    QPushButton *roundingButton;
    QLineEdit *roundingInputLine;
    QLabel *label_8;
    QLineEdit *roundingInput;
    QLabel *label_9;
    QTextBrowser *roundingOutput;
    QLabel *label_10;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1000, 600);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 1000, 600));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(100);
        sizePolicy.setVerticalStretch(100);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        amount = new QWidget();
        amount->setObjectName(QString::fromUtf8("amount"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(amount->sizePolicy().hasHeightForWidth());
        amount->setSizePolicy(sizePolicy1);
        amountInput = new QTextEdit(amount);
        amountInput->setObjectName(QString::fromUtf8("amountInput"));
        amountInput->setGeometry(QRect(50, 60, 900, 310));
        QFont font;
        font.setPointSize(12);
        amountInput->setFont(font);
        amountButton = new QPushButton(amount);
        amountButton->setObjectName(QString::fromUtf8("amountButton"));
        amountButton->setGeometry(QRect(80, 400, 171, 71));
        amountButton->setFont(font);
        label = new QLabel(amount);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(340, 410, 121, 41));
        label->setFont(font);
        amountOutput = new QLabel(amount);
        amountOutput->setObjectName(QString::fromUtf8("amountOutput"));
        amountOutput->setGeometry(QRect(460, 400, 491, 71));
        amountOutput->setFont(font);
        label_6 = new QLabel(amount);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(50, 20, 121, 41));
        label_6->setFont(font);
        tabWidget->addTab(amount, QString());
        palindrome = new QWidget();
        palindrome->setObjectName(QString::fromUtf8("palindrome"));
        sizePolicy1.setHeightForWidth(palindrome->sizePolicy().hasHeightForWidth());
        palindrome->setSizePolicy(sizePolicy1);
        palindromeButton = new QPushButton(palindrome);
        palindromeButton->setObjectName(QString::fromUtf8("palindromeButton"));
        palindromeButton->setGeometry(QRect(80, 400, 171, 71));
        palindromeButton->setFont(font);
        label_2 = new QLabel(palindrome);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(340, 410, 121, 41));
        label_2->setFont(font);
        palindromeOutput = new QLabel(palindrome);
        palindromeOutput->setObjectName(QString::fromUtf8("palindromeOutput"));
        palindromeOutput->setGeometry(QRect(450, 390, 491, 71));
        palindromeOutput->setFont(font);
        palindromeInput = new QTextEdit(palindrome);
        palindromeInput->setObjectName(QString::fromUtf8("palindromeInput"));
        palindromeInput->setGeometry(QRect(50, 60, 900, 310));
        palindromeInput->setFont(font);
        label_7 = new QLabel(palindrome);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(50, 20, 121, 41));
        label_7->setFont(font);
        tabWidget->addTab(palindrome, QString());
        encrypt = new QWidget();
        encrypt->setObjectName(QString::fromUtf8("encrypt"));
        sizePolicy1.setHeightForWidth(encrypt->sizePolicy().hasHeightForWidth());
        encrypt->setSizePolicy(sizePolicy1);
        encryptButton = new QPushButton(encrypt);
        encryptButton->setObjectName(QString::fromUtf8("encryptButton"));
        encryptButton->setGeometry(QRect(80, 400, 171, 71));
        encryptButton->setFont(font);
        encryptInput = new QTextEdit(encrypt);
        encryptInput->setObjectName(QString::fromUtf8("encryptInput"));
        encryptInput->setGeometry(QRect(50, 60, 400, 310));
        encryptInput->setFont(font);
        label_3 = new QLabel(encrypt);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(50, 20, 191, 41));
        label_3->setFont(font);
        label_4 = new QLabel(encrypt);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(540, 20, 261, 41));
        label_4->setFont(font);
        encryptOutput = new QTextBrowser(encrypt);
        encryptOutput->setObjectName(QString::fromUtf8("encryptOutput"));
        encryptOutput->setGeometry(QRect(540, 60, 400, 310));
        encryptOutput->setFont(font);
        encruptInputLine = new QLineEdit(encrypt);
        encruptInputLine->setObjectName(QString::fromUtf8("encruptInputLine"));
        encruptInputLine->setGeometry(QRect(300, 430, 451, 41));
        encruptInputLine->setFont(font);
        label_5 = new QLabel(encrypt);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(300, 390, 281, 41));
        label_5->setFont(font);
        tabWidget->addTab(encrypt, QString());
        rounding = new QWidget();
        rounding->setObjectName(QString::fromUtf8("rounding"));
        sizePolicy1.setHeightForWidth(rounding->sizePolicy().hasHeightForWidth());
        rounding->setSizePolicy(sizePolicy1);
        roundingButton = new QPushButton(rounding);
        roundingButton->setObjectName(QString::fromUtf8("roundingButton"));
        roundingButton->setGeometry(QRect(80, 400, 171, 71));
        roundingButton->setFont(font);
        roundingInputLine = new QLineEdit(rounding);
        roundingInputLine->setObjectName(QString::fromUtf8("roundingInputLine"));
        roundingInputLine->setGeometry(QRect(300, 430, 451, 41));
        roundingInputLine->setFont(font);
        label_8 = new QLabel(rounding);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(300, 390, 281, 41));
        label_8->setFont(font);
        roundingInput = new QLineEdit(rounding);
        roundingInput->setObjectName(QString::fromUtf8("roundingInput"));
        roundingInput->setGeometry(QRect(70, 100, 851, 51));
        roundingInput->setFont(font);
        label_9 = new QLabel(rounding);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(70, 60, 281, 41));
        label_9->setFont(font);
        roundingOutput = new QTextBrowser(rounding);
        roundingOutput->setObjectName(QString::fromUtf8("roundingOutput"));
        roundingOutput->setGeometry(QRect(70, 230, 851, 51));
        roundingOutput->setFont(font);
        label_10 = new QLabel(rounding);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(70, 190, 281, 41));
        label_10->setFont(font);
        tabWidget->addTab(rounding, QString());
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1000, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        amountButton->setText(QApplication::translate("MainWindow", "\320\237\320\276\321\201\321\207\320\270\321\202\320\260\321\202\321\214", nullptr));
        label->setText(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", nullptr));
        amountOutput->setText(QString());
        label_6->setText(QApplication::translate("MainWindow", "\320\241\321\202\321\200\320\276\320\272\320\260:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(amount), QApplication::translate("MainWindow", "\320\232\320\276\320\273\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \321\201\320\276\320\263\320\273\320\260\321\201\320\275\321\213\321\205,\320\263\320\273\320\260\321\201\320\275\321\213\321\205 \320\270 \320\262\321\201\320\265\320\263\320\276 \320\261\321\203\320\272\320\262", nullptr));
        palindromeButton->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\262\320\265\321\200\320\270\321\202\321\214", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", nullptr));
        palindromeOutput->setText(QString());
        label_7->setText(QApplication::translate("MainWindow", "\320\241\321\202\321\200\320\276\320\272\320\260:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(palindrome), QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\262\320\265\321\200\320\272\320\260 \320\275\320\260 \320\277\320\260\320\273\320\270\320\275\320\264\321\200\320\276\320\274", nullptr));
        encryptButton->setText(QApplication::translate("MainWindow", "\320\227\320\260\321\210\320\270\321\204\321\200\320\276\320\262\320\260\321\202\321\214", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "\320\230\321\201\321\205\320\276\320\264\320\275\321\213\320\271 \321\202\320\265\320\272\321\201\321\202:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\272\320\276\320\264\320\270\321\200\320\276\320\262\320\260\320\275\320\275\321\213\320\271 \321\202\320\265\320\272\321\201\321\202:", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "\320\241\320\264\320\262\320\270\320\263 U \320\264\320\273\321\217 \321\210\320\270\321\204\321\200\320\276\320\262\320\260\320\275\320\270\321\217:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(encrypt), QApplication::translate("MainWindow", "\320\250\320\270\321\204\321\200 \321\206\320\265\320\267\320\260\321\200\321\217", nullptr));
        roundingButton->setText(QApplication::translate("MainWindow", "\320\236\320\272\321\200\321\203\320\263\320\273\320\270\321\202\321\214", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\273-\320\262\320\276 \320\267\320\275\320\260\320\272\320\276\320\262 \320\277\320\276\321\201\320\273\320\265 \320\267\320\260\320\277\321\217\321\202\320\276\320\271:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "\320\230\321\201\321\205\320\276\320\264\320\275\320\276\320\265 \321\207\320\270\321\201\320\273\320\276:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "\320\236\320\272\321\200\321\203\320\263\320\273\321\221\320\275\320\275\320\276\320\265 \321\207\320\270\321\201\320\273\320\276:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(rounding), QApplication::translate("MainWindow", "\320\236\320\272\321\200\321\203\320\263\320\273\320\265\320\275\320\270\320\265 \321\207\320\270\321\201\320\273\320\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
