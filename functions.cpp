#include "functions.h"
/**
*\file
 *\brief
 *\author Sergei Baenkov
 *\version 1.0
*/

/**
 *\brief Функция проверки строки на то, чтобы в ней были только русские буквы и знаки припинания
 *\param[in] str Cтрока которую надо проверить
 *\return Возвращает true - если строка удовлетворяет условиям
 */
bool isStrOk (QString &str)
{
    QString allChars = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ .!?,-";
    for (int i = 0;i<str.size() ;i++)
    {
        if (allChars.indexOf(str.at(i)) == -1)
        {
            return false;
        }
    }

    //Провека на то есть ли буквы в этой строке
    allChars.remove(allChars.indexOf(' '),6);
    int amountLetter = 0;
    for (int i = 0;i<str.size() ;i++)
    {
        if (allChars.indexOf(str.at(i)) != -1)
        {
            amountLetter++;
        }
    }
    if (amountLetter == 0)
    {
        return false;
    }
    return true;
}


/**
 *\brief Функция кодирования строки алгоритмом Цезаря со сдвигом u
 *\param[in] str Cтрока которую надо закодировать
 *\param[in] u Число сдвига
 *\return Возвращает значение int: 0 - если строка пуста
 *                                -1 - если в строке находятся недопустимые символы
 *                                -2 - если число сдвига меньше 1
 *                                 1 - при успешном завершении кодирования
 */
int encryption(QString &str,int u)
{
    if (str.isEmpty())
    {
       return 0; // Если строка пуста, возвращает 0
    }
    if (u<1)
    {
        return -2; // Если число сдвига меньше 1 возвращает -2
    }
    if (!isStrOk(str))
    {
        return -1; // Если в строке находятся недопустимые символы, возвращает -1
    }
    QString smallChars = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    QString bigChars = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

    int size = str.size();
    for (int i = 0;i<size ;i++)
    {
        if (smallChars.indexOf(str.at(i)) != -1)
        {
            int newIndex = (smallChars.indexOf(str.at(i)) + u ) % 32;
            str[i] = smallChars[newIndex];
        }
        else if (bigChars.indexOf(str.at(i)) != -1)
        {
            int newIndex = (bigChars.indexOf(str.at(i)) + u ) % 32;
            str[i] = bigChars[newIndex];
        }
    }
    return 1; // При успешном завершении работы возвращает 1
}

/**
 *\brief Функция проверки строки на палиндром
 *\param[in] str Cтрока которую надо проверить
 *\return Возвращает значение int :-2 - если строка пуста
 *                                 -1 - если есть недопустимые символы
 *                                  0 - если не палиндром
 *                                  1 - если палиндром
 */
int isPalindrome(QString &str)
{
    if (str.isEmpty())
    {
        return -2; // Если строка пуста
    }
    str.remove(QChar('\n'));
    if (!isStrOk(str))
    {
        return -1; // Если в строке есть недопустимые символы
    }

    str.remove(QChar(' '));
    str.remove(QChar('.'));
    str.remove(QChar('!'));
    str.remove(QChar('?'));
    str.remove(QChar(','));
    str.remove(QChar('-'));


    for (int i = 0;i<str.size() ;i++)
    {
        if (!str.at(i).isLower())
        {
            str[i] = str.at(i).toLower();
        }
    }

    QString::iterator begin = str.begin();
    QString::iterator end = str.end()-1;
    for (int i = 0;i<str.size() ;i++)
    {
        if (*begin != *end)
        {
            return 0; // Это не палиндром
        }
        begin++;
        end--;
    }

    return 1; // Это палиндром
}

/**
 *\brief Функция подсчёта гласных (включая ё)
 *\param[in] str Cтрока в которой нужно подсчитать кол-во гласных
 *\return Возвращает значение int :-1 - если строка пуста, int > 0 если подсчитано успешно
 */
int amountOfVowels (QString &str)
{
    if (str.isEmpty())
    {
        return -1; // Если строка пуста
    }
    QString Vowels = "аеёиоуыэюяАЕЁИОУЫЭЮЯ";
    int amount = 0;
    for (int i = 0;i<str.size() ;i++)
    {
        if (Vowels.indexOf(str.at(i)) != -1)
        {
            amount++;
        }
    }
    return amount;
}

/**
 *\brief Функция подсчёта согласных
 *\param[in] str Cтрока в которой нужно подсчитать кол-во согласных
 *\return Возвращает значение int :-1 - если строка пуста, int > 0 если подсчитано успешно
 */
int amountOfConsonants (QString &str)
{
    if (str.isEmpty())
    {
        return -1; // Если строка пуста
    }
    QString Consonants = "бвгджзйклмнпрстфхцчшщБВГДЖЗЙКЛМНПРСТФХЦЧШЩ"; //Без ЬЪ
    int amount = 0;
    for (int i = 0;i<str.size() ;i++)
    {
        if (Consonants.indexOf(str.at(i)) != -1)
        {
            amount++;
        }
    }
    return amount;
}

/**
 *\brief Функция подсчёта букв(гласные согласные и "Ь,Ъ")
 *\param[in] str Cтрока в которой нужно подсчитать кол-во согласных
 *\return Возвращает значение int :-1 - если строка пуста, int > 0 если подсчитано успешно
 */
int amountOfCharacter (QString &str)
{
    if (str.isEmpty())
    {
        return -1; // Если строка пуста
    }
    QString Chars = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    int amount = 0;
    for (int i = 0;i<str.size() ;i++)
    {
        if (Chars.indexOf(str.at(i)) != -1)
        {
            amount++;
        }
    }
    return amount;
}

/**
 *\brief Функция округления десятичного числа до заданного кол-ва знаков после запятой
 *\param[in] str Исходное число в строке QString
 * \param[in] amountNumbers - кол-во знаков после запятой после округления
 *\return Возвращает значение int :0 - если строка пуста
 *                                 -1 - если в строке есть недопустимые символы
 *                                  -2 - если колличество знаков после запятой неверное
 *                                   -3 - если в начале стоит не + или -
 *                                    -4 - если в строке больше знаков чем нужно
 *                                     -5 - если больше одной или не одной точки
 *                                      1 - успешное округление
 */
int shorteringNumber (QString &str, int amountNumbers)
{
    if (str.isEmpty())
    {
        return 0; // Если строка пуста
    }
    if ((str.size() - str.indexOf('.') - 1) < amountNumbers || amountNumbers < 1)
    {
        return -2; // Если колличество знаков после запятой неверное
    }
    QString allChars = "1234567890.-+";
    QString sign = "+-";
    int amountDots = 0;
    int amountSign = 0;
    for (int i = 0;i<str.size() ;i++)
    {
        if (allChars.indexOf(str.at(i)) == -1 )
        {
            i = str.size();
            return -1; //Если в строке есть недопустимые символы
        }
        if (str.at(i) == '+' || str.at(i) == '-')
        {
            amountSign++;
        }
        if (str.at(i) == '.')
        {
            amountDots++;
        }
    }
    if (amountSign > 1)
    {
        return -4; // Если в строке больше знаков чем нужно
    }
    if (amountSign == 1 )
    {
        amountNumbers--;
        if (sign.indexOf(str.at(0)) == -1)
        {
            return -3;
        }
    }

    if (amountDots != 1)
    {
        return -5;  // Если в строке больше одной или не одной точки
    }
    QString tmp = QString::number(str.toDouble(),'g',str.size()-1);
    if (amountSign == 1)
    {
        if (str.indexOf(tmp) == -1)
        {
            return -6;
        }
    }
    else
    {
        if (str != tmp)
        {
            return -6;
        }
    }

    str = QString::number(str.toDouble(),'g',str.indexOf('.')+amountNumbers); // 'g' - выводимый формат, число после
                                                                              // него кол-во значащих цифр в числе


    return 1; // При уcпешном выполнении округления
}



