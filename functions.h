#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QCoreApplication>

bool isStrOk(QString &str);
int encryption(QString &str,int u);
int isPalindrome(QString &str);
int amountOfVowels (QString &str);
int amountOfConsonants (QString &str);
int amountOfCharacter (QString &str);
int shorteringNumber (QString &str, int amountNumbers);

#endif // FUNCTIONS_H
