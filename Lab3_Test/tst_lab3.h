#ifndef TST_LAB3_H
#define TST_LAB3_H

#include <QtTest>
#include <QtTest/QtTest>
#include <../functions.h>

class Lab3_Tests : public QObject
{
    Q_OBJECT

public:
    Lab3_Tests();
    ~Lab3_Tests();

private slots:
    void test_encryption();
    void test_isPalindrome();
    void test_amountOfVowels();
    void test_amountOfConsonants();
    void test_amountOfCharacter();
    void test_shorteringNumber();
};

bool compareStr (QString &str1, QString &str2);
#endif // TST_LAB3_H
