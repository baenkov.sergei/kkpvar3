#include "tst_lab3.h"

Lab3_Tests::Lab3_Tests()
{

}

Lab3_Tests::~Lab3_Tests()
{

}

bool compareStr (QString &str1, QString &str2)
{
    if(str1 == str2)
    {
       return true;
    }
    else
    {
        return false;
    }
}


void Lab3_Tests::test_encryption()
{
    QString test_str_before = "Здарова";
    QString test_str_after = "Лздфтёд";
    int message = encryption(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = " Здарова ";
    test_str_after = " Лздфтёд ";
    message = encryption(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = " Здарова.?-! ";
    test_str_after = " Лздфтёд.?-! ";
    message = encryption(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "аА";
    test_str_after = "дД";
    message = encryption(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "";
    test_str_after = "";
    message = encryption(test_str_before,4);
    QCOMPARE(true,compareStr(test_str_before,test_str_after));
    QCOMPARE(0,message);

    test_str_before = " ";
    test_str_after = " ";
    message = encryption(test_str_before,4);
    QCOMPARE(true,compareStr(test_str_before,test_str_after));
    QCOMPARE(-1,message);

    test_str_before = "Здарова";
    test_str_after = "Здарова";
    message = encryption(test_str_before,0);
    QCOMPARE(true,compareStr(test_str_before,test_str_after));
    QCOMPARE(-2,message);

    test_str_before = "123123asdasdЗдарова";
    test_str_after = "123123asdasdЗдарова";
    message = encryption(test_str_before,4);
    QCOMPARE(true,compareStr(test_str_before,test_str_after));
    QCOMPARE(-1,message);

    test_str_before = "!!!!!";
    test_str_after = "!!!!!";
    message = encryption(test_str_before,4);
    QCOMPARE(true,compareStr(test_str_before,test_str_after));
    QCOMPARE(-1,message);
}

void Lab3_Tests::test_isPalindrome()
{
    QString test_str_before = "Анна";
    QString test_str_after = "анна";
    int message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "А роза упала на лапу Азора";
    test_str_after = "арозаупаланалапуазора";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "!А !роза упала на лапу Азора";
    test_str_after = "арозаупаланалапуазора";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "ksjdfhkjsdfА роза упала на лапу Азора";
    test_str_after = "ksjdfhkjsdfА роза упала на лапу Азора";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-1,message);

    test_str_before = "lksdhgksdf";
    test_str_after = "lksdhgksdf";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-1,message);

    test_str_before = "";
    test_str_after = "";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-2,message);

    test_str_before = " ";
    test_str_after = " ";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-1,message);

    test_str_before = "!!!!!";
    test_str_after = "!!!!!";
    message = isPalindrome(test_str_before);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-1,message);
}

void Lab3_Tests::test_amountOfVowels()
{
    QString test_str = "";
    QCOMPARE (-1,amountOfVowels(test_str));

    test_str = " ";
    QCOMPARE (0,amountOfVowels(test_str));

    test_str = "аАбБвВ";
    QCOMPARE (2,amountOfVowels(test_str));

    test_str = "asdsdfksdbf!@#$$%^^&";
    QCOMPARE (0,amountOfVowels(test_str));

    test_str = "Hello Привет";
    QCOMPARE (2,amountOfVowels(test_str));

    test_str = "-1";
    QCOMPARE (0,amountOfVowels(test_str));

}

void Lab3_Tests::test_amountOfConsonants()
{
    QString test_str = "";
    QCOMPARE (-1,amountOfConsonants(test_str));

    test_str = " ";
    QCOMPARE (0,amountOfConsonants(test_str));

    test_str = "аАбБвВ";
    QCOMPARE (4,amountOfConsonants(test_str));

    test_str = "asdsdfksdbf!@#$$%^^&";
    QCOMPARE (0,amountOfConsonants(test_str));

    test_str = "Hello Привет";
    QCOMPARE (4,amountOfConsonants(test_str));

    test_str = "123";
    QCOMPARE (0,amountOfConsonants(test_str));
}

void Lab3_Tests::test_amountOfCharacter()
{
    QString test_str = "";
    QCOMPARE (-1,amountOfCharacter(test_str));

    test_str = " ";
    QCOMPARE (0,amountOfCharacter(test_str));

    test_str = "аАбБвВ";
    QCOMPARE (6,amountOfCharacter(test_str));

    test_str = "asdsdfksdbf!@#$$%^^&";
    QCOMPARE (0,amountOfCharacter(test_str));

    test_str = "Hello Привет";
    QCOMPARE (6,amountOfCharacter(test_str));

    test_str = "123";
    QCOMPARE (0,amountOfCharacter(test_str));
}

void Lab3_Tests::test_shorteringNumber()
{
    QString test_str_before = "192.123455";
    QString test_str_after = "192.123455";
    int message = shorteringNumber(test_str_before,6);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "192.123455";
    test_str_after = "192.1235";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "-192.123455";
    test_str_after = "-192.1235";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "+192.123455";
    test_str_after = "192.1235";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (1,message);

    test_str_before = "1+92.123455";
    test_str_after = "1+92.123455";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-3,message);

    test_str_before = "";
    test_str_after = "";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (0,message);

    test_str_before = "19.2.123455";
    test_str_after = "19.2.123455";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-5,message);

    test_str_before = "192.123455";
    test_str_after = "192.123455";
    message = shorteringNumber(test_str_before,9);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-2,message);

    test_str_before = "192.123455";
    test_str_after = "192.123455";
    message = shorteringNumber(test_str_before,-9);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-2,message);

    test_str_before = "192123455";
    test_str_after = "192123455";
    message = shorteringNumber(test_str_before,4);
    QCOMPARE (true,compareStr(test_str_before,test_str_after));
    QCOMPARE (-5,message);
}
