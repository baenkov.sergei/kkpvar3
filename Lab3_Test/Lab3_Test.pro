QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_lab3.cpp \
    main.cpp

HEADERS += tst_lab3.h

SOURCES +=../functions.cpp

HEADERS +=../functions.h
