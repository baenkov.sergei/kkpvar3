#ifndef ERROR_H
#define ERROR_H

#include <QDialog>

namespace Ui {
class Error;
}

class Error : public QDialog
{
    Q_OBJECT

public:
    explicit Error(QWidget *parent = nullptr);
    ~Error();
    void changeText(QString err);

private slots:
    void on_errorButton_clicked();

private:
    Ui::Error *ui;
};

void errWindow(QString err);

#endif // ERROR_H
