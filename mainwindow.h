#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setError(QString err);

private slots:
    void on_amountButton_clicked();

    void on_palindromeButton_clicked();

    void on_encryptButton_clicked();

    void on_roundingButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
