#include "error.h"
#include "ui_error.h"

/**
*\file
 *\brief
 *\author Sergei Baenkov
 *\version 1.0
*/

Error::Error(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Error)
{
    ui->setupUi(this);
}

Error::~Error()
{
    delete ui;
}

void Error::on_errorButton_clicked()
{
    this->close();
}

void Error::changeText(QString err)
{
    ui->errorStr->setText(err);
}
